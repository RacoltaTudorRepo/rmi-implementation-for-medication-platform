package GUI;


import interfacing.pillDispenser.RMI_Interface;
import model.Patient;

import javax.swing.*;

public class Login {
    private JFrame frmOgin;
    private JTextField textField;
    private JPasswordField textField_1;
    private RMI_Interface rmi_interface;


    public Login() {

        frmOgin = new JFrame();
        frmOgin.setTitle("Login");
        frmOgin.setBounds(100, 100, 450, 300);
        frmOgin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmOgin.getContentPane().setLayout(null);
        frmOgin.setVisible(true);

        textField = new JTextField();
        textField.setBounds(94, 103, 86, 20);
        frmOgin.getContentPane().add(textField);
        textField.setColumns(10);

        textField_1 = new JPasswordField();
        textField_1.setBounds(229, 103, 86, 20);
        frmOgin.getContentPane().add(textField_1);
        textField_1.setColumns(10);

        JLabel lblName = new JLabel("Username");
        lblName.setBounds(121, 80, 46, 14);
        frmOgin.getContentPane().add(lblName);

        JLabel lblPassword = new JLabel("Password");
        lblPassword.setBounds(249, 80, 46, 14);
        frmOgin.getContentPane().add(lblPassword);


        JButton btnLogin = new JButton("Login");
        btnLogin.setBounds(162, 193, 89, 23);
        frmOgin.getContentPane().add(btnLogin);

        btnLogin.addActionListener((event) -> {
            System.out.println(textField.getText()+" "+ new String(textField_1.getPassword()));
            Patient patient = rmi_interface.loginPatient(textField.getText(),new String(textField_1.getPassword()));
            System.out.println(patient.getName());
            Dispenser dispenser=new Dispenser();
            dispenser.setPatient(patient);
            dispenser.setRmiInterface(rmi_interface);
            dispenser.setGUIVisible();

        });
    }

    public RMI_Interface getRmi_interface() {
        return rmi_interface;
    }

    public void setRmi_interface(RMI_Interface rmi_interface) {
        this.rmi_interface = rmi_interface;
    }

}
