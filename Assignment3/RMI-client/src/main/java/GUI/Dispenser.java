package GUI;

import interfacing.pillDispenser.RMI_Interface;
import model.Medication;
import model.MedicationPlan;
import model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class Dispenser {
    public JFrame frmUserPanel;
    private JTable table;
    private String[] columns = {"ID","Name", "Side effects", "Dosage","Intake intervals"};
    private DefaultTableModel model;
    private Object[][] data;

    private Patient patient;
    private RMI_Interface rmiInterface;
    private Time time;
    private Timer timer;
    private JLabel label;
    private JTextField input;
    private JButton button;
    private boolean loaded;

    private DispenserController controller;


    public Dispenser(){
        loaded=false;
        frmUserPanel = new JFrame();
        frmUserPanel.setBounds(100, 100, 770, 570);
        frmUserPanel.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frmUserPanel.setTitle("Dispenser");
        frmUserPanel.getContentPane().setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(82, 58, 341, 223);
        frmUserPanel.getContentPane().add(scrollPane);

        table = new JTable();
        model = new DefaultTableModel(data, columns);
        table.setModel(model);;
        scrollPane.setViewportView(table);
        time=Time.valueOf("07:59:55");

        timer=new Timer(1000,e -> { {
                time.setTime(time.getTime()+1000);
                System.out.println(time.toString());
                if(time.toString().equals("08:00:00")){
                    controller.loadMedicationPlans(patient,model,rmiInterface);
                    loaded=true;
                }
                label.setText("Time: "+time.toString());

                if(loaded){
                    controller.checkTaken(time);
                }

            }
        });

        timer.start();
        controller=new DispenserController(model);

        label=new JLabel("Time: "+time.toString());
        label.setBounds(600,150,100,100);
        frmUserPanel.getContentPane().add(label);

        input=new JTextField();
        input.setBounds(500,250,70,30);
        frmUserPanel.getContentPane().add(input);

        button=new JButton("Take");
        button.setBounds(600,250,100,30);
        frmUserPanel.getContentPane().add(button);
        button.addActionListener(e -> {
            controller.validateChoice(input,time);
        });
    }



    public void setRmiInterface(RMI_Interface rmiInterface) {
        this.rmiInterface = rmiInterface;
    }

    public void setGUIVisible(){
        frmUserPanel.setVisible(true);
        controller.setRmi_interface(rmiInterface);
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
        controller.setPatientID(patient.getID());
    }
}
