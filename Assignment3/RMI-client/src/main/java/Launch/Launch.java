package Launch;

import GUI.Dispenser;
import GUI.Login;
import interfacing.pillDispenser.RMI_Interface;
import model.MedicationPlan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

@ComponentScan({"GUI"})
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@SpringBootApplication
public class Launch {
    public static void main(String[] args) throws RemoteException, NotBoundException {
//        Registry registry= LocateRegistry.getRegistry(1888);
//
//        RMI_Interface service=(RMI_Interface) registry.lookup("RMIService");
//        List<MedicationPlan> medicationPlans=service.getMedicationPlans(new Long("8"));
//        System.out.println(medicationPlans.size());

        SpringApplicationBuilder builder = new SpringApplicationBuilder(Launch.class);
        builder.headless(false);
        RMI_Interface service = builder.run(args).getBean(RMI_Interface.class);
        Login login = new Login();
        login.setRmi_interface(service);
    }


    @Bean
    RmiProxyFactoryBean service() {
        RmiProxyFactoryBean rmiProxyFactory = new RmiProxyFactoryBean();
        rmiProxyFactory.setServiceUrl("rmi://localhost:1888/RMI_Interface");
        rmiProxyFactory.setServiceInterface(RMI_Interface.class);
        return rmiProxyFactory;
    }


}
