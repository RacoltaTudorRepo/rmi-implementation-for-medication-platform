package rest_services;

import model.Caregiver;
import model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import repository.CaregiverRepository;
import repository.PatientRepository;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("backend/caregiver")
public class CaregiverService {
    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private CaregiverRepository caregiverRepository;

    @PostMapping("/getCaregiver")
    public Caregiver getCaregiver(@RequestBody String name){
        return caregiverRepository.findCaregiverByUsername(name);
    }

    @PostMapping("/getPatientsForCaregiver")
    public List<Patient> getPacientsForCaregiver(@RequestBody Long caregiverID){
        return patientRepository.findAllBycaregiver_id(caregiverID);
    }


}
