package model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@DiscriminatorColumn(name="PATIENT")
public class Patient extends User implements Serializable {

    private String medicalRecord;

    private boolean isCared;

    public Patient() {
    }

    public Patient(String username, String password, String name, Date birthDate, String gender, String adress, String medicalRecord) {
        super(username,password, name, birthDate, gender, adress,"Patient");
        this.medicalRecord = medicalRecord;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "medicalRecord='" + medicalRecord + '\'' +
                '}';
    }

    public boolean isCared() {
        return isCared;
    }

    public void setCared(boolean cared) {
        isCared = cared;
    }
}
