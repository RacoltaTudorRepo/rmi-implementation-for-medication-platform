package interfacing.pillDispenser;

import model.Medication;
import model.MedicationPlan;
import model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import repository.MedicationPlanRepository;
import repository.MessageRepository;
import repository.PatientRepository;

import java.util.List;

@Component
public class RMIService implements RMI_Interface {

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public List<MedicationPlan> getMedicationPlans(Long patientID) {
        return medicationPlanRepository.findAllByPatientID(patientID);
    }

    @Override
    public Patient loginPatient(String username, String password) {
        List<Patient> patients=(List<Patient>)patientRepository.findAll();
        for(Patient patient:patients){
            if(patient.getUsername().equals(username) && patient.getPassword().equals(password))
                return patient;
        }
        return null;
    }

    @Override
    public void saveMessage(Medication medication, boolean taken, String reason,Long patientID){
        Message message=new Message(medication.getName(),taken,reason,patientID);
        messageRepository.save(message);
    }
}
