package security;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class LoggingFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException

    {

        HttpServletRequest req = (HttpServletRequest) request;
        //System.out.println(req.getHeader("role"));
        //System.out.println(req.getMethod() + " " +req.getRequestURI());

        chain.doFilter(request, response);
    }
}
